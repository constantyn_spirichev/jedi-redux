'use strict'
var path = require('path');
var webpack = require('webpack');
var entryPoint = [
  'webpack-dev-server/client?http://localhost:3000'
];

module.exports = {
  entry: "./app/index",
  output: {
    path: __dirname + "/build",
    filename: "app.js",
    publicPath: __dirname + "/public/"
  },
  resolve: {
    extensions: ['', '.js'],
    root: path.join(__dirname, 'app')
  },
  watch: true,
  watchOptions: {
    aggregateTimeout: 200
  },
  devtool: '#source-map',
  module:{
    loaders: [
      {
        test: /\.js$/,
        loader: "babel",
        include: path.join(__dirname, 'app')
      }
    ]

  }
};
