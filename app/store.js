import {createStore, applyMiddleware} from 'redux';
import reducer, {getAppConditions} from './reducer';
import logger from './middlewares/logger';
import async from './middlewares/async';
import ws from './middlewares/ws';

const [appIdle, appUp, appDown] = getAppConditions().types;
const location = {id: null, name: "not arrived"};
const app = {
  condition: appDown,
  offset: 0,
  requests: [],
  url: "http://jedi.smartjs.academy/dark-jedis/3616",
};

const createStoreWithMiddleware = applyMiddleware(logger, async, ws)(createStore)


export default createStoreWithMiddleware(reducer, {location, app, jedisList: []});
