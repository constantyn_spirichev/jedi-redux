const APP_STARTS = "APP_STARTS";
const APP_NEW_REQUEST = "APP_NEW_REQUEST";

const LOCATION_UPDATE = "LOCATION_UPDATE";
const LOCATION_MATCHED = "LOCATION_MATCHED";

const JEDI_FETCHED = "JEDI_FETCHED";
const JEDI_FETCH = "JEDI_FETCH";

const MASTER_FETCH = "MASTER_FETCH";
const APRENTICE_FETCH = "APRENTICE_FETCH";
const MASTER_FETCHED = "MASTER_FETCHED";
const APRENTICE_FETCHED = "APRENTICE_FETCHED";

const APP_IDLE = "APP_IDLE";
const APP_UP = "APP_UP";
const APP_DOWN = "APP_DOWN";

const INIT_JEDI_URL = "http://jedi.smartjs.academy/dark-jedis/3616";

export default function reducer(oldState, action) {
  console.log("Reducer-> ", action.type)
  switch (action.type) {
    case LOCATION_UPDATE:
      return {
        ...oldState,
        location: action.data,
      };
    case JEDI_FETCH:
      var condition = action.data ? action.data.condition : oldState.app.condition;

      return {
        ...oldState,
        app: {
          ...oldState.app,
          condition
        }
      };
    case JEDI_FETCHED:
      let jedisList = [...oldState.jedisList];
      let appCondition = oldState.app.condition;
      let appUrl = oldState.app.url;

      if(appCondition === APP_DOWN){

        jedisList.push(action.data);
        appUrl = action.data.apprentice.url;

      }
      if(appCondition === APP_UP){

        jedisList.unshift(action.data);
        appUrl = action.data.master.url;
      }

      return {
        ...oldState,
        app: {
          ...oldState.app,
          condition: appCondition,
          url: appUrl,
          requests: oldState.app.requests.filter(item => item.id != action.data.id),
        },
        jedisList,
      };
    case APP_NEW_REQUEST:
      let requests = [...oldState.app.requests];
          requests.push(action.data);

      return {
        ...oldState,
        app: {
          ...oldState.app,
          requests,
        }
      };
    default:
      return oldState;
  }
};

export function locationUpdate(){
  return {
    type: LOCATION_UPDATE
  }
}

export function masterFetch(){
  return {
    type: MASTER_FETCH,
  }
}

export function jediFetched(data){
  return {
    type: JEDI_FETCHED,
    data
  }
}

export function appStart(){
  return {
    type: APP_STARTS
  }
}

export function jediFetch(){
  return {
    type: JEDI_FETCH
  }
}

export function appNewRequest(){
  return {
    type: APP_NEW_REQUEST
  }
}

export function getAppConditions(){
  return {
    types: [
      APP_IDLE,
      APP_UP,
      APP_DOWN
    ]
  }
}
export function initJediUrl(){
  return INIT_JEDI_URL
}
