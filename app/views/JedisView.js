import _ from "underscore";
import BaseView from './BaseView';

export default class JedisView extends BaseView {

  initialize(){
    this._template = _.template(document.getElementById("jedisListItem").innerHTML);
  }

  render(list) {
    if(typeof this._prevState === "undefined") {
      this._prevState = list;
    }

    if(this._prevState === list || list.length == 0){

    } else {
      const template = this._template;
      const slots = this._node.querySelectorAll(".css-slot");

      this._prevState = list;
      
      Array.prototype.map.call(slots, (slot, index)=>{
        if(list[index]){
          slot.innerHTML = template(list[index]);
          slot.id = list[index].id;
        } else {
          slot.innerHTML = "";
          slot.id = "";
        }
      });
  }

  }
}
