import BaseView from './BaseView'

export default class LocationView extends BaseView {
  render(location) {
    this._node.innerText = location.name;
  }
}
