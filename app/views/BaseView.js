export default class BaseView {
  constructor(node, actions = {}) {
    this._node = node;
    this._actions = actions;
    this.initialize();
  }

  initialize(){}

  render(data) {
    if (!this._rendered) {
      this.initialRender();
    }
  }
}
