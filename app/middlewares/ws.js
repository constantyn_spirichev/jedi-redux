import reducer, { locationUpdate, appStart } from '../reducer';

const middleware = store => next => action => {

  const update = locationUpdate().type;
  const start = appStart().type
  if(action.type === start) {
    const ws = new WebSocket('ws://jedi.smartjs.academy');

    ws.addEventListener('message', (e)=>{store.dispatch(
            {
                type: update,
                data: JSON.parse(e.data),
            }
        )
    });
  }

  //console.log("ws MW:", action);
    next(action);
}

export default middleware;
