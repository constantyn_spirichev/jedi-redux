const middleware = store => next => action => {
  console.log(action.type, action, store.getState());
  return next(action);
}

export default middleware;
