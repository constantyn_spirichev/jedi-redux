import {getAppConditions, jediFetched, jediFetch, appNewRequest, initJediUrl} from '../reducer';
const middleware = store => next => action => {
  const [appIdle, appUp, appDown] = getAppConditions().types;
  //console.log(store.getState());
  const state = store.getState();
  let   condition = action.data ? action.data.condition : state.app.condition;


  if(action.type == jediFetch().type){
    console.log("--> ", condition)
    switch (condition) {
      case appDown:
      console.log("-> ", appDown)
        var lastJedi = state.jedisList[state.jedisList.length-1]
        var url = lastJedi ? lastJedi.apprentice.url : initJediUrl();
        var id = lastJedi ? lastJedi.apprentice.id : 3616; // @TODO pass it as const via import

        if(url && state.jedisList.length < 5){
          let request = fetch(url);
          store.dispatch({
            type: appNewRequest().type,
            data: {
              id,
              request,
              condition: appDown,
            }
          });

          request = request.then(r => r.json()).then(
            data => {
              store.dispatch(jediFetched(data));
              store.dispatch({type: jediFetch().type, data:{condition}});
            }
          );

        }
        return next(action);
        case appUp:
          console.log("-> ", appUp)
          var url = state.jedisList[0].master.url;
          if(url && state.jedisList.length < 5){
            let request = fetch(url);
            store.dispatch({type: appNewRequest().type, data: request});

            request = request.then(r => r.json()).then(
              data => {
                store.dispatch(jediFetched(data));
                store.dispatch({type: jediFetch().type, data:{condition}});

              }
            );

          }
          return next(action);
      default:
        return next(action);

    }
  }
  return next(action);
}

export default middleware;
