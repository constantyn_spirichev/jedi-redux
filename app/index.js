// rating.smartjs.academy/rating
import reducer, {locationUpdate, appStart, jediFetch, getAppConditions} from './reducer';
import store from './store';
import JedisView from './views/JedisView';
import LocationView from './views/LocationView';
import connect from './connect';
//console.log(reducer, locationUpdate, appStart );
const [appIdle, appUp, appDown] = getAppConditions().types;

const locationView = connect(
  store,
  new LocationView(document.getElementById("location").querySelector('.planet-name')),
  (state) => state.location
);

const jedisList = connect(
  store,
  new JedisView(document.getElementById("jedis")),
  (state) => state.jedisList
);


store.dispatch(appStart());
store.dispatch(jediFetch());


document.querySelector('.css-button-up').addEventListener("click", function(e){
  store.dispatch({type: jediFetch().type, data: {condition: appUp}});
})
